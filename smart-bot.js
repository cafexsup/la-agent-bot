"use strict";

/**
 * 
 * This bot illustrates how to build a simple agent bot, suitable for Live Assist for Dynamics
 * 365. It builds on the transfer-bot.js, and offers a trivial knowledge base, accessed through
 * the use of keywords found in source messages. Note - this is a laughable simple implementation,
 * and is no substitute for a proper language processing.
 * 
 * You may benefit from looking at LUIS, see: https://www.luis.ai
 *
 * To use this bot, simply replace the content of your 'index.js' with this.
 */


var builder = require("botbuilder");
var botbuilder_azure = require("botbuilder-azure");
var path = require('path');

const TRANSFER_MESSAGE = 'transfer to ';

// trivial knowledge base implementation:
const KNOWLEDGE_BASE = {
    'cafex':        `Caf�X software transforms business collaboration and customer engagement in mobile 
                     and web applications. Find out more at: https://cafex.com`,
    'live assist':  `With Live Assist, your business can provide personalized live assistance within web 
                     pages and apps through chat, co-browse and video support. Omnichannel service helps 
                     you resolve issues faster and increase customer satisfaction. Find out more at: 
                     https://www.liveassistfor365.com/en/`,
    'bots':         `Microsoft Bot Framework allows you to build and connect intelligent bots to interact 
                     with your users naturally wherever they are � from your website or app to Cortana, 
                     Skype, Teams, Office 365 mail, Slack, Facebook Messenger Skype for Business and more...`
};

var useEmulator = (process.env.NODE_ENV == 'development');

var connector = useEmulator ? new builder.ChatConnector() : new botbuilder_azure.BotServiceConnector({
    appId: process.env['MicrosoftAppId'],
    appPassword: process.env['MicrosoftAppPassword'],
    stateEndpoint: process.env['BotStateEndpoint'],
    openIdMetadata: process.env['BotOpenIdMetadata']
});

var bot = new builder.UniversalBot(connector);
bot.localePath(path.join(__dirname, './locale'));

bot.dialog('/', function (session) {
    
    switch(session.message.sourceEvent.type)
    {
        case "visitorContextData":
            // process context data if required. This is the first message received so say hello.
            session.send('Hi, I am clever bot! Feel free to ask me about: ' + Object.keys(KNOWLEDGE_BASE).join(', ') + '.');
            break;

        case "systemMessage":
            // react to system messages if required
            break;

        case "transferFailed":
            // react to transfer failures if required
            session.send('Sorry - something went wrong when trying to transfer you.');
            break;

        case "otherAgentMessage":
            // react to messages from a supervisor if required
            break;

        case "visitorMessage":
            // Check for transfer message
			if(session.message.text.startsWith(TRANSFER_MESSAGE)) 
            {
				var transferTo = session.message.text.substr(TRANSFER_MESSAGE.length);
				var msg = new builder.Message(session).sourceEvent({directline: {type: "transfer", agent: transferTo}});
				session.send(msg);
			} 
            else 
            {
                // turn to lowercase so we are not case sensitive
                let lowercaseMessage = session.message.text.toLowerCase();
                for (let keyword in KNOWLEDGE_BASE)
                {
                    if (lowercaseMessage.includes(keyword))
                    {
                        // the message includes a keyword we are interested in
                        // so send the KB info!
                        session.send(KNOWLEDGE_BASE[keyword]);
                    }
                }
			}
            break;

        default:
            session.send('This is not a Live Assist message ' + session.message.sourceEvent.type);
    }
    
});

if (useEmulator) {
    var restify = require('restify');
    var server = restify.createServer();
    server.listen(3978, function() {
        console.log('test bot endpont at http://localhost:3978/api/messages');
    });
    server.post('/api/messages', connector.listen());    
} else {
    module.exports = { default: connector.listen() }
}